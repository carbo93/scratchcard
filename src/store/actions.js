export default {
    setBalance(state, num) {
        commit('setBalance', num) 
    },
    setUserData(state, user) {
        state.commit('setUserData', user) 
    },
    setWalletData(state, wallet) {
        state.commit('setWalletData', wallet) 
    },
    
    addShoppingCartItem(state, item) {

        state.commit('addShoppingCartItem', item) 
    },
    removeShoppingCartItem(state, item) {

        state.commit('removeShoppingCartItem', item) 
    },
    resetShoppingCart(state) {

        state.commit('resetShoppingCartItem') 

    },
    playBackgroundAudio(state){

        state.commit('playBackgroundAudio') 

    },
    stopBackgroundAudio(state){

        state.commit('stopBackgroundAudio') 

    },
    
}