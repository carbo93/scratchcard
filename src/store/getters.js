export default {
    count: state => state.count, //取得state裡面的內容
    getBalance(state){
        return state.balance
    },
    getUserData(state){
        return state.userData
    },
    getWalletData(state){
        return state.walletData
    },
    getShoppingCart(state){
        return state.shoppingCart
    },
    getToken(state) {
        return state.jwtToken
    },
    getBackgroundAudioStatus(state) {
        return state.backgroundAudioStatus
    },
}