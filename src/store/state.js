export default {
    balance: 0,
    /* 玩家資訊 */
    userData: {},
    /* 錢包 */
    walletData:{},
    /* 購物車 */
    shoppingCart:[],
    /* 背景音樂 */
    backgroundAudio:'',
    backgroundAudioStatus:true,
}