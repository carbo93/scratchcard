import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import state from './state'
import mutations from './mutations'
import getters from './getters'
//import shoppingcart from "./game/shoppingcart"


Vue.use(Vuex)

export default new Vuex.Store({
    actions,
    state,
    mutations,
    getters,
    /*
    game:{
        shoppingcart,
    }
    */
    struct: true
})