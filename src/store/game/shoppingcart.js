const state = {
    shoppingCart:[]
}

const getters = {
    getShoppingCart(state){
        return state.shoppingCart
    },
}

const actions = {

    addShoppingCartItem(state, item) {

        state.commit('addShoppingCartItem', item) 
    },
    removeShoppingCartItem(state, item) {

        state.commit('removeShoppingCartItem', item) 
    },

}

const mutations = {

    addShoppingCartItem(state, item) {
        state.shoppingCart.push(item)
    }, 
    removeShoppingCartItem(state, item_id) {

        for (let i = 0; i < state.shoppingCart.length; i++) {
            if(state.shoppingCart[i].sn == item_id){

                state.shoppingCart.splice(i, 1);
            } 
        }

    }, 
    
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}