#!/bin/bash

#writer forking.ch
USER='horizon'
SSH_KEY='/home/horizon/.ssh/id_rsa'
#SSH_KEY='/wh/.id_rsa'
DEPLOY_IP='192.168.128.190'
GITLAB_IP='192.168.128.182'
DEV_IP='192.168.128.188'
DEST_IP=$1
DEST_PORT='22'
GIT_FOLDER='roulette'
GIT_URL='git@git-v.winhua.com:frontend/roulette.git'
WEB_PATH='/var/www/html'
BRANCH_NAME=$2

if [ "$BRANCH_NAME" == "master" ];
then
curl -X POST --data-urlencode "payload={\"text\": \"Start building $BRANCH_NAME code \"}" https://hooks.slack.com/services/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
#deploy to 192.168.128.183
sudo ssh -i ${SSH_KEY} -p ${DEST_PORT} ${DEST_IP} -f "cd ${WEB_PATH} && git clone ${GIT_URL} -b $BRANCH_NAME && cd ${GIT_FOLDER} && npm install && npm run build && cp -r dist ../ && cd .. && chown -R ${USER}.${USER} dist && rm -rf ${WEB_PATH}/${GIT_FOLDER} && sudo -u horizon /bin/bash /home/horizon/deploy-ft.sh && echo 'complete!'"

elif [ "$BRANCH_NAME" == "v_03" ]; then
curl -X POST --data-urlencode "payload={\"text\": \"Start building $BRANCH_NAME code \"}" https://hooks.slack.com/services/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
sudo ssh -i ${SSH_KEY} -p ${DEST_PORT} ${USER}@${DEV_IP} -f "cd /var/www/${BRANCH_NAME} && git clone ${GIT_URL} -b $BRANCH_NAME && cd ${GIT_FOLDER} && npm install && npm run build && cp -r dist ../ && cd .. && chown -R ${USER}.${USER} dist && rm -rf /var/www/${BRANCH_NAME}/${GIT_FOLDER} && echo 'complete!'"
elif [ "$BRANCH_NAME" == "wilson" ]; then
curl -X POST --data-urlencode "payload={\"text\": \"Start building $BRANCH_NAME code \"}" https://hooks.slack.com/services/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
sudo ssh -i ${SSH_KEY} -p ${DEST_PORT} ${USER}@${DEV_IP} -f "cd /var/www/${BRANCH_NAME} && git clone ${GIT_URL} -b $BRANCH_NAME && cd ${GIT_FOLDER} && npm install && npm run build && cp -r dist ../ && cd .. && chown -R ${USER}.${USER} dist && rm -rf /var/www/${BRANCH_NAME}/${GIT_FOLDER} && echo 'complete!'"
elif [ "$BRANCH_NAME" == "eason" ]; then
curl -X POST --data-urlencode "payload={\"text\": \"Start building $BRANCH_NAME code \"}" https://hooks.slack.com/services/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
sudo ssh -i ${SSH_KEY} -p ${DEST_PORT} ${USER}@${DEV_IP} -f "cd /var/www/${BRANCH_NAME} && git clone ${GIT_URL} -b $BRANCH_NAME && cd ${GIT_FOLDER} && npm install && npm run build && cp -r dist ../ && cd .. && chown -R ${USER}.${USER} dist && rm -rf /var/www/${BRANCH_NAME}/${GIT_FOLDER} && echo 'complete!'"

fi